/**
 * @license Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function (config) {
	config.allowedContent = true
	config.uploadUrl = window.location.origin + "/api/system/oss/upload"
	// config.exportPdf_tokenUrl = "/api/system/oss/upload"
	config.toolbar = [
		{
			name: "document",
			items: ["Source", "-"],
		},
		{ name: "clipboard", items: ["PasteText", "PasteFromword", "-"] },
		{ name: "editing", items: ["Find", "Replace", "-"] },
		{ name: "forms", items: ["Form", "TextField", "Textarea", "Select", "HiddenField"] },
		{
			name: "paragraph",
			items: [
				"NumberedList",
				"BulletedList",
				"-",
				"Outdent",
				"Indent",
				"-",
				"Blockquote",
				"-",
				"JustifyLeft",
				"JustifyCenter",
				"JustifyRight",
				"JustifyBlock",
				"-",
				"BidiLtr",
				"BidiRtl",
			],
		},
		{ name: "links", items: ["Link", "Unlink", "Anchor"] },
		{
			name: "insert",
			items: [
				"addpic" /* Image */,
				,
				"Flash",
				"Table",
				"HorizontalRule",
				"Smiley",
				"SpecialChar",
				"PageBreak",
				"Iframe",
			],
		},
		{
			name: "basicstyles",
			items: [
				"Bold",
				"Italic",
				"Underline",
				"Strike ",
				"Subscript",
				"Superscript ",
				" - ",
				"CopyFormatting ",
				"RemoveFonmat",
			],
		},
		{ name: "styles", items: ["Format", "Font", "FontSize"] },
		{ name: "colors", items: ["TextColor", "BGColor"] },
		{ name: "tools", items: ["Maximize", "ShowBlocks"] },
	]
	config.extraPlugins = "addpic"
}
