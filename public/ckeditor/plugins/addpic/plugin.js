;(function () {
	//Section 1 : 按下自定义按钮时执行的代码
	var a = {
		exec: function (editor) {
			editor.open_dialog && editor.open_dialog(editor)
		},
	}
	CKEDITOR.plugins.add("addpic", {
		init: function (editor) {
			editor.addCommand("addpic", a)
			editor.ui.addButton("addpic", {
				label: "添加图片",
				icon: this.path + "addpic.png",
				command: "addpic",
			})
		},
	})
})()
