window.DefaultGlobalSetting = {
	dark: false,
	navigatorType: "Top Left",
	tab: {
		visible: true,
		type: "shape",
	},
	copyright: {
		visible: true,
		name: "公司名称222",
		date: "2022-07-23122",
		link: "http://baidu.com2212",
		record: "备案号12121",
	},
	componentType: {
		size: "default",
	},
	watermark: {
		visible: false,
		text: "水印",
		type: "text",
		url: "",
		content:
			"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAL4AAAB4CAYAAABMxUg0AAAAAXNSR0IArs4c6QAABTNJREFUeF7tmrGvDUEUh3+vpNVIJKJTSlQSeok/gAKdhFZCh4JKpUHoJCgU9NQkWqVOJEqNglbmmpHJvN29Zu6e5MT5NC/e3T33nO/3vb2zc3dP/INAQAJ7AWdmZAgI8ZEgJAHEDxk7QyM+DoQkgPghY2doxMeBkAQQP2TsDI34OBCSAOKHjJ2hER8HQhJA/JCxMzTi40BIAogfMnaGRnwcCEkA8UPGztCIjwMhCSB+yNgZGvFxICQBxA8ZO0MjPg6EJID4IWNnaMTHgZAEED9k7AyN+DgQkgDih4ydoREfB0ISQPyQsTM04uNASAKIHzJ2hkZ8HAhJAPFDxs7QiI8DIQkgfsjYGRrxcSAkAcQPGTtDIz4OhCSA+CFjZ2jEx4GQBBA/ZOwMjfg4EJIA4oeMnaERHwdCEkD8kLEzNOLjQEgCiB8ydoZGfBwISQDxQ8bO0Ii/rgMHJD3IJa9L+rVueaqtRQDx1yL5p84a4l+U9CK39UnSBUmf122Taoi/rgMj4teip26eSqo/LUrNqwutXpL0ct1R/u9qiL9uvtvEPy7pXLUcSu+exL+cf36faGep5qEs/HPE7wsS8ft4bTu6SHpqYolSJD3cvIb426gavI74fVDTFfuVpBMTp6Xlxpt8NU/LkjuS7uXjlv4gEL8vg1WORvzdMdbLjVr8t3n58rP6Yzgj6UPzloi/ewbdFRC/G9m+E05LepSXL18rydOB6cp/Mv+cuwFF/N0z6K6A+N3I9p1wW9KRvBOTXiz7+D8k3cxHL+26tLs69RukT40rkm7lX7bfDXBzO5gf4g+Cy6e14tU7MK8lvZO0bauRK/5uGQydjfhD2P6elKRNV/XyJVO79XhDUloKpeOmtipTIcTfLYOhsxF/CNvmpHK1Tzer7e5Nej0tS47mXaB09S/HtO+I+OMZDJ+J+MPoNlfq+mqfKk192VTW8FM7Olzxx/nvdCbij+Er+/ntlXxK/PK7YzNLnrkrfvpEeSjpvqRr3NyOBTV3FuL381z6Mmru8YKyLPrSPIczdcUvnxDlAbWyRVqWT/UTn+zq9Oe3OQPx+8EVMad2a5aeqymfEh8b+dvtzPLFV7kZXqqZbpzfS5pbRvVPF+QMxO8LuojWPkFZqhRJv83czLbyH8wPl52V1Arf1kz/fyLpWfPIRP1oRN80gY9G/L7wk7jnJT2e2Z78l6VHe3+Q/pjS+n/useJtT3z2TcDRLHVWciB9c3u3qjX3aTD6dog/Sm7hPK74BlAp6Z8A4vvPiA4NCCC+AVRK+ieA+P4zokMDAohvAJWS/gkgvv+M6NCAAOIbQKWkfwKI7z8jOjQggPgGUCnpnwDi+8+IDg0IIL4BVEr6J4D4/jOiQwMCiG8AlZL+CSC+/4zo0IAA4htApaR/AojvPyM6NCCA+AZQKemfAOL7z4gODQggvgFUSvongPj+M6JDAwKIbwCVkv4JIL7/jOjQgADiG0ClpH8CiO8/Izo0IID4BlAp6Z8A4vvPiA4NCCC+AVRK+ieA+P4zokMDAohvAJWS/gkgvv+M6NCAAOIbQKWkfwKI7z8jOjQggPgGUCnpnwDi+8+IDg0IIL4BVEr6J4D4/jOiQwMCiG8AlZL+CSC+/4zo0IAA4htApaR/AojvPyM6NCCA+AZQKemfAOL7z4gODQggvgFUSvongPj+M6JDAwKIbwCVkv4JIL7/jOjQgADiG0ClpH8CiO8/Izo0IID4BlAp6Z8A4vvPiA4NCCC+AVRK+ieA+P4zokMDAohvAJWS/gn8Biodwnnogz+YAAAAAElFTkSuQmCC",
	},
}
