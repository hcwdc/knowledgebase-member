const { cwd } = require("process")
const { resolve } = require("path")
const { statSync, readdirSync } = require("fs")

const resolvedPath = {}

/**
 * @description 根据路径拼接src相对路径
 * @param {string} path 路径
 * @returns {string}
 */
function _resolve(path) {
	return resolvedPath[path] || (resolvedPath[path] = resolve(cwd(), "src", path))
}

/**
 * @description 判断是否是目录
 * @param {string} path
 * @returns {boolean}
 */
function isDir(path) {
	return statSync(_resolve(path)).isDirectory()
}

/**
 * @description 获取目录下的所有文件及文件夹
 * @param {string} path
 * @returns {Array<string>}
 */
function traversingDir(path) {
	return readdirSync(_resolve(path))
}

/**
 * @description 获取目录下的所有文件夹
 * @param {Array<string>} pathArray
 * @param {string} path
 * @returns {Array<string>}
 */
function getDirs(pathArray, path) {
	return pathArray.filter(item => isDir(path + "/" + item))
}

/**
 * @description 驼峰转短路径
 * @param {string} name
 * @returns {string}
 */
function hump2short(name = "") {
	name = name[0].toLowerCase() + name.substring(1)
	return name.replace(/([A-Z])/g, (_match, p1) => "-" + p1.toLowerCase())
}

/**
 * @description 短路径转驼峰
 * @param {string} name
 * @returns {string}
 */
function short2hump(name = "") {
	return name.replace(/(\-([a-z]))/g, (_match, _p1, p2) => p2.toUpperCase())
}

/**
 * @description 短路径转大驼峰
 * @param {string} name
 * @returns {string}
 */
function short2Hump(name = "") {
	name = name[0].toUpperCase() + name.substring(1)
	return name.replace(/(\-([a-z]))/g, (_match, _p1, p2) => p2.toUpperCase())
}

module.exports = {
	resolve: _resolve,
	isDir,
	traversingDir,
	getDirs,
	hump2short,
	short2hump,
	short2Hump,
}
