const { parse } = require("path")
const { writeFileSync } = require("fs")
const { resolve, traversingDir } = require("./module")

const rootDir = resolve("../public/icons")
const iconEntrance = resolve("./plugins/icons/static.js")

function generate() {
	const iconArray = traversingDir(rootDir)

	let importString = `const icons = {\n`
	iconArray.forEach(icon => {
		const iconNameObject = parse(icon)
		const name = iconNameObject.name
		const extname = iconNameObject.ext.replace(/^\./, "")

		importString += `	"${name}": "${extname}",\n`
	})
	importString += `}\n
export default icons`

	writeFileSync(iconEntrance, importString, { encoding: "utf-8" })
}

module.exports = function traversalIcons() {
	return {
		name: "traversalIcons",
		options() {
			console.log("开始生成图标!")
			generate()
			console.log("生成图标结束!")
		},
		buildStart() {
			console.log("开始生成图标!")
			generate()
			console.log("生成图标结束!")
		},
	}
}
