import { createRouter, createWebHistory } from "vue-router"
// import { createRouterList } from "./router"
import { useHistoryStore } from "/store/history"
import { useMenuStore } from "/store/menu"
import Home from "../examples/main/index.vue"
import { ElMessage } from "element-plus"
const routes = [
	{
		path: "/",
		component: Home,
		name: "Home",
		children: [
			{
				path: "/",
				name: "Index",
				component: () => import("../views/index.vue"),
			},
			{
				path: "/uploads",
				name: "uploads",
				component: () => import("../views/uploads/index.vue"),
			},
			{
				path: "/viewPdf",
				name: "viewPdf",
				component: () => import("../views/person/viewPdf.vue"),
			},
		],
	},
	// 后台组织架构
	{
		path: "/organizational",
		component: () => import("../views/backstage/organizational/index.vue"),
		name: "organizational",
		redirect: "/organizational",
		children: [
			{
				path: "/organizationalPermissions",
				name: "organizationalPermissions",
				component: () => import("../views/backstage/organizational/organizationalPermissions.vue"),
				meta: {
					name: "组织架构",
					isHouTai: true,
				},
			},
			{
				path: "/group",
				name: "group",
				component: () => import("../views/backstage/organizational/group.vue"),
				meta: {
					name: "群组",
					isHouTai: true,
				},
			},
			{
				path: "/assignment",
				name: "assignment",
				component: () => import("../views/backstage/organizational/assignment.vue"),
				meta: {
					name: "权限管理",
					isHouTai: true,
				},
			},
		],
	},
	// 后台应用中心
	{
		path: "/applicationCenter",
		component: () => import("../views/backstage/applicationCenter/index.vue"),
		name: "applicationCenter",
		redirect: "/knowledgeClassification",
		children: [
			{
				path: "/knowledgeClassification",
				name: "knowledgeClassification",
				component: () => import("../views/backstage/applicationCenter/classification.vue"),
				meta: {
					name: "知识分类",
					isHouTai: true,
				},
			},
			{
				path: "/themeKnowledgeBase",
				name: "themeKnowledgeBase",
				component: () => import("../views/backstage/applicationCenter/themeKnowledgeBase.vue"),
				meta: {
					name: "主题知识库",
					isHouTai: true,
				},
			},
			{
				path: "/knowledgeLabels",
				name: "knowledgeLabels",
				component: () => import("../views/backstage/applicationCenter/labels.vue"),
				meta: {
					name: "知识标签",
					isHouTai: true,
				},
			},
			{
				path: "/knowledgeBanner",
				name: "knowledgeBanner",
				component: () => import("../views/backstage/applicationCenter/knowledgeBanner.vue"),
				meta: {
					name: "首页配图",
					isHouTai: true,
				},
			},
			{
				path: "/knowledgeBeCurrent",
				name: "applicationCenterBasis",
				component: () => import("../views/backstage/applicationCenter/beCurrent/index.vue"),
				meta: {
					name: "通用模块",
					isHouTai: true,
				},
				children: [
					{
						path: "",
						component: () => import("../views/backstage/applicationCenter/beCurrent/prompt.vue"),
						name: "通用模块子",
						meta: {
							name: "通用模块",
							// isHouTai: true,
						},
					},
					{
						path: "agreement",
						component: () => import("../views/backstage/applicationCenter/beCurrent/details.vue"),
						meta: {
							name: "使用协议",
							// isHouTai: true,
						},
					},
					{
						path: "download",
						component: () => import("../views/backstage/applicationCenter/beCurrent/download.vue"),
						meta: {
							name: "文件下载",
							// isHouTai: true,
						},
					},
				],
			},
		],
	},
	// 后台属性模板
	{
		path: "/attribute",
		component: () => import("../views/backstage/attribute/index.vue"),
		name: "attribute",
		redirect: "/attributeTemplate",
		children: [
			{
				path: "/attributeTemplate",
				name: "attributeTemplate",
				component: () => import("../views/backstage/attribute/template.vue"),
				meta: {
					name: "属性模板",
					isHouTai: true,
				},
			},
		],
	},

	// 个人中心
	{
		path: "/person",
		component: () => import("../views/person/index.vue"),
		name: "person",
		redirect: "/personHome",
		children: [
			{
				path: "/personHome",
				name: "personHome",
				component: () => import("../views/person/home.vue"),
				meta: {
					name: "个人首页",
				},
			},
			{
				path: "/personSubscribe",
				name: "personSubscribe",
				component: () => import("../views/person/subscribe.vue"),
				meta: {
					name: "我的订阅",
				},
			},
			{
				path: "/personKnowledge",
				name: "personKnowledge",
				component: () => import("../views/person/knowledge.vue"),
				meta: {
					name: "我的知识",
				},
			},
			{
				path: "/personCollect",
				name: "personCollect",
				component: () => import("../views/person/collect.vue"),
				meta: {
					name: "我的收藏",
				},
			},
			{
				path: "/personSetUp",
				name: "personSetUp",
				component: () => import("../views/person/setUp.vue"),
				meta: {
					name: "个人设置",
				},
			},
			{
				path: "/personPassword",
				name: "personPassword",
				component: () => import("../views/person/password.vue"),
				meta: {
					name: "密码设置",
				},
			},
			{
				path: "/personJournal",
				name: "personJournal",
				component: () => import("../views/person/journal.vue"),
				meta: {
					name: "日志记录",
				},
			},
			{
				path: "/personDuplicate",
				name: "personDuplicate",
				component: () => import("../views/person/duplicate.vue"),
				meta: {
					name: "查重记录",
				},
			},
		],
	},
	{
		path: "/dengLu",
		component: () => import("../views/dengLu/index.vue"),
		name: "dengLu",
	},
	{
		path: "/privacy",
		component: () => import("../views/privacy/index.vue"),
		name: "privacy",
	},
	{
		path: "/knowledge",
		component: () => import("../views/knowledge/index.vue"),
		name: "knowledge",
	},
	{
		path: "/knowledgeList",
		component: () => import("../views/knowledge/knowledgeList.vue"),
		name: "knowledgeList",
	},
	{
		path: "/knowledgeEditing",
		component: () => import("../views/knowledge/editing.vue"),
		name: "knowledgeEditing",
	},
	{
		path: "/knowledgeDetails",
		component: () => import("../views/knowledge/details.vue"),
		name: "knowledgeDetails",
	},
	{
		path: "/knowledgeCreation",
		component: () => import("../views/knowledge/creation.vue"),
		name: "knowledgeCreation",
	},
	{
		path: "/video",
		component: () => import("../views/video/index.vue"),
		name: "video",
	},
	{
		path: "/videoDetails",
		component: () => import("../views/video/details.vue"),
		name: "videoDetails",
	},
	{
		path: "/addVideo",
		component: () => import("../views/video/addVideo.vue"),
		name: "addVideo",
	},
	{
		path: "/search",
		component: () => import("../views/search/index copy.vue"),
		name: "search",
	},
	// {
	// 	path: "/searchssss",
	// 	component: () => import("../views/search/index copy.vue"),
	// 	name: "search",
	// },
	{
		path: "/:catchAll(.*)",
		redirect: "/",
	},
]
function scrollBehavior(_to, _from, savedPosition) {
	if (savedPosition) {
		return savedPosition
	} else {
		return { x: 0, y: 0 }
	}
}
const router = createRouter({
	history: createWebHistory("/"),
	routes,
	scrollBehavior,
})
router.beforeEach((to, from) => {
	if (to.meta.title) {
		document.title = `${to.meta.title} - ${import.meta.env.VITE_APP_NAME}`
	} else {
		document.title = import.meta.env.VITE_APP_NAME
	}
	const historyStore = useHistoryStore()
	historyStore.addHistory({
		path: to.path,
		fullPath: to.fullPath || to.path,
		title: to.meta.title || "",
	})
	if (from.meta?.close_leave) {
		historyStore.deleteHistory(from.path)
	}
	const menuStore = useMenuStore()
	const route = menuStore.findInMenuList(to.path)
	if (route) {
		menuStore.nowCurrent = route.parent.indexs[1]
	}
	if (to.path != "/dengLu" && to.path != "/privacy") {
		if (!window.localStorage.getItem("token")) {
			return { path: "/dengLu" }
		}
		return
	}
	return true
})
export default router
