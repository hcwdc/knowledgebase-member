import { createApp } from "vue"
import App from "./App.vue"

import "@/assets/style/element.scss"
import ElementPlus from "element-plus"
// import "element-plus/dist/index.css"
// import "element-plus/theme-chalk/dark/css-vars.css"

import head from "@/components/head/index.vue"
import myfoot from "@/components/myfoot/index.vue"
import Components from "./components"
import "./components/index.scss"

import VMdPreview from "@kangc/v-md-editor/lib/preview"
import "@kangc/v-md-editor/lib/style/preview.css"
import githubTheme from "@kangc/v-md-editor/lib/theme/github.js"
import "@kangc/v-md-editor/lib/theme/style/github.css"
import "./assets/icon/iconfont.css"

import hljs from "highlight.js"
import VueCropper from "vue-cropper"
import "vue-cropper/dist/index.css"

VMdPreview.use(githubTheme, {
	Hljs: hljs,
})

import Axios from "./axios"
import pinia from "./store"
import Router from "./router" // 路由
import Directive from "./directive"
import Icons from "./plugins/icons" // Element-plus 图标

const app = createApp(App)

app.component("myhead", head)
app.component("myfoot", myfoot)
app.use(ElementPlus)
app.use(Components)
app.use(Axios)
app.use(pinia)
app.use(Router)
app.use(Directive)
app.use(Icons)
app.use(VMdPreview)
app.use(VueCropper)

app.mount("#app")
