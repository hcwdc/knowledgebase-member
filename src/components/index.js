import VBumen from "./bumen/index.vue"
import VCheckbox from "./checkbox/index.vue"
import VContent from "./content/index.vue"
import VCopy from "./copy/index.vue"
import VCropper from "./cropper/index.vue"
import VDatePicker from "./date-picker/index.vue"
import VDepartment from "./department/index.vue"
import VEditor from "./editor/index.vue"
import VHead from "./head/index.vue"
import VIcon from "./icon/index.vue"
import VImage from "./image/index.vue"
import VMyfoot from "./myfoot/index.vue"
import VPagination from "./pagination/index.vue"
import VPassword from "./password/index.vue"
import VPictureUpload from "./picture-upload/index.vue"
import VPreviewFile from "./previewFile/index.vue"
import VRadio from "./radio/index.vue"
import VRegion from "./region/index.vue"
import VRegionChecked from "./region-checked/index.vue"
import VSearchBar from "./search-bar/index.vue"
import VSelect from "./select/index.vue"
import VSelectIcon from "./select-icon/index.vue"
import VShowContent from "./show-content/index.vue"
import VTable from "./table/index.vue"
import VTree from "./tree/index.vue"
import VTreeSelect from "./tree-select/index.vue"
import VUpload from "./upload/index.vue"

export default {
	install: app => {
		app.component("v-bumen", VBumen)
		app.component("v-checkbox", VCheckbox)
		app.component("v-content", VContent)
		app.component("v-copy", VCopy)
		app.component("v-cropper", VCropper)
		app.component("v-date-picker", VDatePicker)
		app.component("v-department", VDepartment)
		app.component("v-editor", VEditor)
		app.component("v-head", VHead)
		app.component("v-icon", VIcon)
		app.component("v-image", VImage)
		app.component("v-myfoot", VMyfoot)
		app.component("v-pagination", VPagination)
		app.component("v-password", VPassword)
		app.component("v-picture-upload", VPictureUpload)
		app.component("v-preview-file", VPreviewFile)
		app.component("v-radio", VRadio)
		app.component("v-region", VRegion)
		app.component("v-region-checked", VRegionChecked)
		app.component("v-search-bar", VSearchBar)
		app.component("v-select", VSelect)
		app.component("v-select-icon", VSelectIcon)
		app.component("v-show-content", VShowContent)
		app.component("v-table", VTable)
		app.component("v-tree", VTree)
		app.component("v-tree-select", VTreeSelect)
		app.component("v-upload", VUpload)
	},
}
