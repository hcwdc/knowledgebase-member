import { uploadFile } from "/plugins/file"
export function useUpload(uploadInput) {
	async function httpRequest(files) {
		let file = files ?? uploadInput.value.files
		if (file) {
			const promiseArray = []
			for (let i = 0; i < file.length; i++) {
				const item = file[i]
				promiseArray.push(
					new Promise(async (resolve, reject) => {
						try {
							const result = await uploadFile(item)
							resolve(result)
						} catch (error) {
							reject(error)
						}
					})
				)
			}
			uploadInput.value.value = null
			const result = await Promise.allSettled(promiseArray)
			return result.filter(item => item.status === "fulfilled").map(item => item.value)
		}
		uploadInput.value.value = null
		throw new Error("没有选择文件")
	}
	return {
		httpRequest,
	}
}
