import { shallowRef, watchEffect } from "vue"
const TEXT = "加载中"
export function useLoading(props) {
	let dotNumber = 0
	const loadingText = shallowRef("加载中")
	let timer = null
	function startLoadingTextAnimate() {
		clearInterval(timer)
		timer = setInterval(() => {
			loadingText.value = TEXT + ".".repeat(++dotNumber % 7)
		}, 500)
	}
	function stopLoadingTextAnimate() {
		clearInterval(timer)
		dotNumber = 0
		loadingText.value = TEXT
	}
	watchEffect(() => {
		if (props.loading) {
			startLoadingTextAnimate()
		} else {
			stopLoadingTextAnimate()
		}
	})
	return {
		loadingText,
		startLoadingTextAnimate,
		stopLoadingTextAnimate,
	}
}
