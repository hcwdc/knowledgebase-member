import { nextTick, onUnmounted } from "vue"
let observer = null
export function useFooter(hasFooterSlot, props) {
	watchEffect(async () => {
		if (hasFooterSlot.value) {
			if (!props.loading) {
				await nextTick()
				const footer = document.querySelector(".content-footer")
				if (!footer) return
				observer = new IntersectionObserver(
					([{ intersectionRatio }]) => {
						footer.classList[intersectionRatio < 1 ? "add" : "remove"]("sticky")
					},
					{ threshold: [1] }
				)
				observer.observe(footer)
				return
			}
		}
		observer?.disconnect()
	})
	onUnmounted(() => {
		observer?.disconnect()
	})
}
