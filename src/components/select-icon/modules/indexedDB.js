import { useIndexedDBStore } from "/store/indexedDB"
export async function allIndexedDBIcons() {
	const indexedDBStore = useIndexedDBStore()
	const result = await indexedDBStore.getAll("icons")
	return result.map(item => item.url)
}
export async function setIndexedDBIcon(url) {
	const indexedDBStore = useIndexedDBStore()
	if (await indexedDBStore.querySelect("icons", "url", url)) {
		return { type: "existing" }
	}
	return indexedDBStore.set("icons", { url })
}
export async function removeIndexedDBIcon(url) {
	const indexedDBStore = useIndexedDBStore()
	const data = await indexedDBStore.querySelect("icons", "url", url)
	if (data) {
		return indexedDBStore.delete("icons", data.id)
	}
	return { type: "not-found" }
}
