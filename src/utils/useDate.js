export function useFormatDate(date, str) {
	const format = (date, str) => {
		const o = {
			"y+": date.getFullYear(), //year
			"M+": date.getMonth() + 1, //month
			"d+": date.getDate(), //day
			"h+": date.getHours(), //hour
			"m+": date.getMinutes(), //minute
			"s+": date.getSeconds(), //second
			"q+": Math.floor((date.getMonth() + 3) / 3), //quarter
			"S": date.getMilliseconds(), //millisecond
		}
		if (/(y+)/.test(str)) {
			str = str.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length))
		}
		for (const k in o) {
			if (new RegExp("(" + k + ")").test(str)) {
				str = str.replace(
					RegExp.$1,
					RegExp.$1.length === 1 ? o[k] + "" : ("00" + o[k]).substr(("" + o[k]).length)
				)
			}
		}
		return str
	}
	return format(new Date(date), str)
}
