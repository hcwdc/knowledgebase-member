import { useUserInfoStore } from "/store/userInfo"

export default async (el, { value, dir, arg }) => {
	if (!value) return

	const userInfoStore = useUserInfoStore()

	if (userInfoStore.auth.length) {
		const type = arg || "all"
		el.style.display = "none"
		if (userInfoStore.hasAuth(value, type)) {
			el.style.display = ""
			return
		}
		el.parentNode?.removeChild(el)
		return
	}
	try {
		const auth = await userInfoStore.getAuthList()
		if (auth.length) {
			dir.mounted(el, { value, dir, arg })
		}
	} catch (error) {}
}
