import clipboard from "./clipboard"
import ripple from "./ripple/index"
import auth from "./auth"

export default {
	install: app => {
		app.directive("copy", clipboard)
		app.directive("ripple", ripple)
		app.directive("auth", auth)
	},
}
