import VBumen from "./components/bumen/index.vue"
import VCheckbox from "./components/checkbox/index.vue"
import VContent from "./components/content/index.vue"
import VCopy from "./components/copy/index.vue"
import VCropper from "./components/cropper/index.vue"
import VDatePicker from "./components/date-picker/index.vue"
import VDepartment from "./components/department/index.vue"
import VEditor from "./components/editor/index.vue"
import VHead from "./components/head/index.vue"
import VIcon from "./components/icon/index.vue"
import VImage from "./components/image/index.vue"
import VMyfoot from "./components/myfoot/index.vue"
import VPagination from "./components/pagination/index.vue"
import VPassword from "./components/password/index.vue"
import VPictureUpload from "./components/picture-upload/index.vue"
import VPreviewFile from "./components/previewFile/index.vue"
import VRadio from "./components/radio/index.vue"
import VRegion from "./components/region/index.vue"
import VRegionChecked from "./components/region-checked/index.vue"
import VSearchBar from "./components/search-bar/index.vue"
import VSelect from "./components/select/index.vue"
import VSelectIcon from "./components/select-icon/index.vue"
import VShowContent from "./components/show-content/index.vue"
import VTable from "./components/table/index.vue"
import VTree from "./components/tree/index.vue"
import VTreeSelect from "./components/tree-select/index.vue"
import VUpload from "./components/upload/index.vue"

declare module "vue" {
	export interface GlobalComponents {
		VBumen: typeof VBumen
		VCheckbox: typeof VCheckbox
		VContent: typeof VContent
		VCopy: typeof VCopy
		VCropper: typeof VCropper
		VDatePicker: typeof VDatePicker
		VDepartment: typeof VDepartment
		VEditor: typeof VEditor
		VHead: typeof VHead
		VIcon: typeof VIcon
		VImage: typeof VImage
		VMyfoot: typeof VMyfoot
		VPagination: typeof VPagination
		VPassword: typeof VPassword
		VPictureUpload: typeof VPictureUpload
		VPreviewFile: typeof VPreviewFile
		VRadio: typeof VRadio
		VRegion: typeof VRegion
		VRegionChecked: typeof VRegionChecked
		VSearchBar: typeof VSearchBar
		VSelect: typeof VSelect
		VSelectIcon: typeof VSelectIcon
		VShowContent: typeof VShowContent
		VTable: typeof VTable
		VTree: typeof VTree
		VTreeSelect: typeof VTreeSelect
		VUpload: typeof VUpload
	}
}

const install: (Vue: any) => void
export default {
	install,
}
