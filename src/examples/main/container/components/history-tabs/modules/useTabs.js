import { shallowRef, watchEffect } from "vue"
export function useTabs(historyStore, router, route) {
	const active = shallowRef("")
	function startWatch() {
		return watchEffect(() => {
			active.value = route.fullPath
		})
	}
	function deleteTabs(paneName) {
		historyStore.deleteHistory(paneName)
		if (paneName == route.fullPath) {
			active.value = "/"
			router.push("/")
		}
	}
	function tabChange({ paneName }) {
		router.push(paneName)
	}
	return {
		active,
		startWatch,
		deleteTabs,
		tabChange,
	}
}
