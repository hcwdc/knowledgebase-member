import { computed } from "vue"
import { useMenuStore } from "/store/menu"
export function useBreadcrumb() {
	const menuStore = useMenuStore()
	const breadcrumb = computed(() => {
		const thisPage = menuStore.findInMenuList()
		const breadcrumb = []
		if (!thisPage) {
			return [
				{
					title: "首页",
					path: "/",
				},
			]
		}
		const title = thisPage.parent.title
		const path = thisPage.parent.path
		return breadcrumb.concat(
			...title.map((item, index) => {
				return {
					title: item,
					path: path[index],
				}
			}),
			{
				title: thisPage.title,
				path: thisPage.path,
			}
		)
	})
	return {
		breadcrumb,
	}
}
