import { computed, shallowRef, nextTick } from "vue"
export function usePosition() {
	const x = shallowRef(0)
	const y = shallowRef(0)
	const style = computed(() => ({
		top: y.value + "px",
		left: x.value + "px",
	}))
	async function updatePosition() {
		await nextTick()
		const event = window.event
		const element = document.querySelector("#history-contextmenu")
		const width = element.offsetWidth
		const height = element.offsetHeight
		let left = event.pageX
		let top = event.pageY
		if (event.pageX + width > window.innerWidth) {
			left = event.pageX - width
		}
		if (event.pageY + height > window.innerHeight) {
			top = event.pageY - height
		}
		x.value = left
		y.value = top
	}
	return {
		style,
		updatePosition,
	}
}
