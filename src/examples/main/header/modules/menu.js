import { computed } from "vue"
import { useRouter } from "vue-router"
import { useMenuStore } from "/store/menu"
export function useMenu(navigatorVisible) {
	const menuStore = useMenuStore()
	const router = useRouter()
	const menuList = computed(() => menuStore.menuList)
	const nowCurrent = computed(() => menuStore.nowCurrent)
	function onChangeMenu(menu, index) {
		if (!menu.children && menu.path) {
			router.push(menu.path)
			return
		}
		if (!navigatorVisible.value) {
			menuStore.nowCurrent = index
		} else {
			if (menu.children) {
				const path = getFirstChildrenPath(menu)
				if (path) {
					router.push(path)
				}
			} else if (menu.path) {
				router.push(menu.path)
			}
		}
	}
	function getFirstChildrenPath(menu) {
		if (menu.children && menu.children.length > 0) {
			return getFirstChildrenPath(menu.children[0])
		}
		return menu.path
	}
	return {
		menuList,
		nowCurrent,
		onChangeMenu,
	}
}
