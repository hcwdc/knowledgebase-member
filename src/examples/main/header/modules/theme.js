import { computed } from "vue"
import { useSettingStore } from "/store/setting"
import { HEADER_HEIGHT } from "/store/setting/constants"
export function useTheme() {
	const settingStore = useSettingStore()
	const style = computed(() => ({
		height: HEADER_HEIGHT + "px",
		top: settingStore.headerHeight + "px",
	}))
	const headerVisible = computed(() => settingStore.headerHeight === 0)
	const navigatorVisible = computed(() => settingStore.navigatorType === "Narrow Top")
	const isDark = computed(() => settingStore.dark)
	const popoverEffect = computed(() => (isDark.value ? "dark" : "light"))
	function toggleDarkTheme() {
		settingStore.dark = !settingStore.dark
	}
	return {
		style,
		headerVisible,
		navigatorVisible,
		isDark,
		popoverEffect,
		toggleDarkTheme,
	}
}
