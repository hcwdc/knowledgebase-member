import { computed } from "vue"
export function useMenuTheme(themeStore) {
	const menuTheme = computed({
		get() {
			return themeStore.navigatorType
		},
		set(value) {
			themeStore.navigatorType = value
		},
	})
	function changeMenuTheme(name) {
		menuTheme.value = name
	}
	return {
		menuTheme,
		changeMenuTheme,
	}
}
