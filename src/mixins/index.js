import { shallowRef, ref } from "vue"
import { useList } from "./modules/useList"
import { ElNotification } from "element-plus"
import { getAxios } from "/axios"
function formatIndexModules(module) {
	return {
		pagination: true,
		search: {},
		channel: "api",
		deleteUrl: "",
		exportUrl: "",
		batchDeleteUrl: "",
		beforeGetData: () => ({}),
		afterGetData: () => {},
		...module,
	}
}
// 调取接口
export function useIndex(module) {
	const { pagination, search, channel, indexUrl, deleteUrl, exportUrl, batchDeleteUrl, beforeGetData, afterGetData } =
		formatIndexModules(module)
	const {
		loading,
		tableLoading,
		search: searchData,
		selection,
		tableData,
		reload,
		toSearch,
		toRefreshSearch,
		onSizeChange,
		onCurrentChange,
		toDelete,
		toExport,
		toBatchDelete,
		...basis
	} = useList({
		pagination,
		search,
		deleteUrl,
		exportUrl,
		batchDeleteUrl,
		getData,
	})
	async function getData() {
		let search = Object.assign({}, searchData)
		const execute = await beforeGetData()
		if (execute !== undefined) {
			if (execute === false) {
				return
			}
			search = Object.assign({}, search, execute)
		}
		try {
			tableLoading.value = true
			const page = pagination
				? {
						current: tableData.current,
						size: tableData.size,
				  }
				: {}
			let result = await getAxios(
				indexUrl,
				{
					...search,
					...page,
				},
				{
					channel,
				}
			)
			result = (await afterGetData(result)) || result
			loading.value = false
			tableLoading.value = false
			if (pagination) {
				const resultData = result
				tableData.current = +resultData.current
				tableData.size = +resultData.size
				tableData.total = +resultData.total
				tableData.lastPage = Math.ceil(tableData.total / tableData.size)
				tableData.data = resultData.records
			} else {
				tableData.data = result
			}
		} catch (error) {
			loading.value = false
			tableLoading.value = false
			ElNotification({ type: "error", message: error })
		}
	}
	return {
		...basis,
		loading,
		tableLoading,
		search: searchData,
		selection,
		tableData,
		reload,
		toSearch,
		toRefreshSearch,
		onSizeChange,
		onCurrentChange,
		toDelete,
		toExport,
		toBatchDelete,
	}
}
export function useInfo(modules) {
	const infoRef = shallowRef(null)
	function showInfo(id, query) {
		if (infoRef.value) {
			infoRef.value.init(id, query)
		}
	}
	return {
		infoRef,
		showInfo,
	}
}
function formatTreeModules(modules) {
	return {
		pagination: true,
		search: {},
		channel: "api",
		deleteUrl: "",
		exportUrl: "",
		batchDeleteUrl: "",
		beforeGetData: () => ({}),
		afterGetData: () => {},
		field: "id",
		...modules,
	}
}
export function useTree(modules) {
	const {
		pagination,
		search,
		channel,
		indexUrl,
		deleteUrl,
		exportUrl,
		batchDeleteUrl,
		beforeGetData,
		afterGetData,
		field,
	} = formatTreeModules(modules)
	const {
		loading,
		tableLoading,
		search: searchData,
		selection,
		tableData,
		reload,
		toSearch,
		toRefreshSearch,
		onSizeChange,
		onCurrentChange,
		toDelete,
		toExport,
		toBatchDelete,
		...basis
	} = useList({
		pagination,
		search,
		deleteUrl,
		exportUrl,
		batchDeleteUrl,
		getData,
	})
	const treeChecked = ref("")
	async function getData() {
		let search = Object.assign({}, searchData)
		const execute = await beforeGetData()
		if (execute !== undefined) {
			if (execute === false) {
				return
			}
			search = Object.assign({}, search, execute)
		}
		try {
			tableLoading.value = true
			const page = pagination
				? {
						current: tableData.current,
						size: tableData.size,
				  }
				: {}
			let result = await getAxios(
				indexUrl,
				{
					...search,
					...page,
					[field]: treeChecked.value,
				},
				{
					channel,
				}
			)
			result = (await afterGetData(result)) || result
			loading.value = false
			tableLoading.value = false
			if (pagination) {
				const resultData = result
				tableData.current = +resultData.current
				tableData.size = +resultData.size
				tableData.total = +resultData.total
				tableData.lastPage = Math.ceil(tableData.total / tableData.size)
				tableData.data = resultData.records
			} else {
				tableData.data = result
			}
		} catch (error) {
			loading.value = false
			tableLoading.value = false
			ElNotification({ type: "error", message: error })
		}
	}
	function onTreeClick(value) {
		treeChecked.value = value
		reload()
	}
	return {
		...basis,
		loading,
		tableLoading,
		search: searchData,
		selection,
		tableData,
		treeChecked,
		reload,
		toSearch,
		toRefreshSearch,
		onSizeChange,
		onCurrentChange,
		onTreeClick,
		toDelete,
		toExport,
		toBatchDelete,
	}
}
