import { unref } from "vue"
import { useRouter } from "vue-router"
import { useUserInfoStore } from "/store/userInfo"
export function useBasis() {
	const router = useRouter()
	const userInfoStore = useUserInfoStore()
	function toPage(path, type, query) {
		if (typeof path === "number") {
			router.go(path)
			return
		}
		let routerType = "push"
		if (typeof type === "string") {
			if (type === "replace") {
				routerType = "replace"
			}
		} else {
			query = type
		}
		router[routerType]({
			path,
			query,
		})
	}
	function validateForm(form) {
		return new Promise((resolve, reject) => {
			unref(form).validate((valid, errors) => {
				if (valid) {
					resolve()
				} else {
					reject(errors)
				}
			})
		})
	}
	return {
		userInfo: userInfoStore.userInfo,
		toPage,
		validateForm,
	}
}
