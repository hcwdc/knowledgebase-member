import { unref, onActivated, shallowRef, reactive } from "vue"
import { useRoute } from "vue-router"
import { ElNotification, ElMessageBox } from "element-plus"
import { useIndexedDBStore } from "/store/indexedDB"
import { postAxios } from "/axios"
import { useBasis } from "./basis"
function formatModules(modules) {
	return {
		channel: "api",
		data: {},
		beforeSubmit: () => {},
		key: "",
		...modules,
	}
}
export function useAdd(modules) {
	const route = useRoute()
	const basis = useBasis()
	const IndexedDBStore = useIndexedDBStore()
	const { formData: moduleFormData, channel, submitUrl, data, beforeSubmit, key: pageKey } = formatModules(modules)
	const form = shallowRef()
	const loading = shallowRef(false)
	const formData = reactive(Object.assign({}, moduleFormData))
	const reservedFormData = Object.assign({}, moduleFormData)
	const saveData = reactive(Object.assign({}, data))
	const reservedSaveData = Object.assign({}, data)
	let hasIndexedDBData = false
	function createIndexedDBId() {
		return route.path + pageKey + basis.userInfo.id
	}
	function resetForm() {
		loading.value = false
		form.value?.resetFields()
		Object.keys(reservedFormData).forEach(key => (formData[key] = reservedFormData[key]))
		Object.keys(reservedSaveData).forEach(key => (saveData[key] = reservedSaveData[key]))
	}
	async function getDataForIndexedDB() {
		try {
			hasIndexedDBData = false
			const result = await IndexedDBStore.get("add-edit", createIndexedDBId())
			if (!result) return
			hasIndexedDBData = true
			const resultFormData = JSON.parse(result.formData || "{}")
			Object.keys(resultFormData).forEach(key => (formData[key] = resultFormData[key]))
			const resultData = JSON.parse(result.data || "{}")
			Object.keys(resultData).forEach(key => (saveData[key] = resultData[key]))
		} catch (error) {
			console.log(error)
		}
	}
	async function submit() {
		loading.value = true
		let execute = Object.assign({}, formData)
		if (typeof beforeSubmit === "function") {
			let result = await beforeSubmit()
			if (result === false) {
				loading.value = false
				return
			} else if (result !== undefined) {
				execute = result
			}
		}
		unref(form)
			.validate()
			.then(async () => {
				try {
					await postAxios(submitUrl, execute, { channel })
					ElNotification({ title: "提示", type: "success", message: "添加成功" })
					if (hasIndexedDBData) {
						try {
							await ElMessageBox.confirm("是否要删除已暂存的数据？", "提示", {
								type: "warning",
							})
							IndexedDBStore.delete("add-edit", createIndexedDBId())
						} catch (error) {}
					}
					setTimeout(() => {
						loading.value = false
						basis.toPage(-1)
					}, 500)
				} catch (error) {
					ElNotification({ title: "提示", type: "error", message: error })
					loading.value = false
				}
			})
			.catch(() => {
				loading.value = false
				ElNotification({ title: "提示", type: "error", message: "请检查表单是否正确填写" })
			})
	}
	async function temporary() {
		try {
			await IndexedDBStore.put("add-edit", {
				id: createIndexedDBId(),
				formData: JSON.stringify(formData),
				data: JSON.stringify(saveData),
				date: new Date().getTime(),
			})
			hasIndexedDBData = true
			ElNotification({ title: "提示", type: "success", message: "暂存成功" })
		} catch (error) {
			ElNotification({ title: "提示", type: "error", message: error })
		}
	}
	async function deleteTemporary() {
		try {
			await ElMessageBox.confirm("是否要删除暂存的数据？", "提示", {
				type: "warning",
			})
			IndexedDBStore.delete("add-edit", createIndexedDBId())
			hasIndexedDBData = false
			ElNotification({ title: "提示", type: "success", message: "删除成功" })
		} catch (error) {}
	}
	function activated() {
		resetForm()
		getDataForIndexedDB()
	}
	onActivated(activated)
	return {
		...basis,
		form,
		loading,
		formData,
		reservedFormData,
		data: saveData,
		resetForm,
		submit,
		temporary,
		deleteTemporary,
	}
}
