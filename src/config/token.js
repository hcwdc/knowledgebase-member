// Token Config
export const token = () => "Bearer " + window.localStorage.getItem("token") || ""
export const refreshToken = () => "Bearer " + window.localStorage.getItem("refreshToken") || ""
export const tokenName = "Authorization"
export const refreshTokenName = "token"
