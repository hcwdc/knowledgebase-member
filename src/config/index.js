export * from "./token"
export * from "./indexedDB"
export * from "./axios"
export * from "./upload"

export const title = "Admin"
