// Axios Config
export const axiosTimeout = 1000 * 60 * 10
export const axiosChannelHeader = ["/api", "/code"]
export const axiosRefreshUrl = "/api/rep/refreshToken"
export const axiosRefreshRequestType = "POST"
export const axiosSuccessCode = 0
export const axiosRefreshCode = 1000
export const axiosExpiredCode = 401
