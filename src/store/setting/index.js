import { defineStore } from "pinia"
import { ASIDE_WIDTH, HEADER_HEIGHT, LEFT_WIDTH } from "./constants"
const setting = window.DefaultGlobalSetting
if (setting.dark) {
	document.querySelector("html")?.classList.add("dark")
}
export const useSettingStore = defineStore("setting", {
	state() {
		return {
			...setting,
			fullPage: false,
			visible: false,
			isMobile:false
		}
	},
	actions: {
		setIsMobile(isMobile){
			this.isMobile=isMobile
		}
	},
	getters: {
		headerHeight() {
			if (this.fullPage) {
				return HEADER_HEIGHT * -1
			}
			if (["Top Left", "Narrow Top"].includes(this.navigatorType)) {
				return 0
			}
			return HEADER_HEIGHT * -1
		},
		leftCoordinate() {
			if (this.fullPage) {
				if (this.navigatorType === "Double Left") {
					return (LEFT_WIDTH + ASIDE_WIDTH) * -1
				} else if (["Top Left", "Single Left"].includes(this.navigatorType)) {
					return ASIDE_WIDTH * -1
				}
			}
			return 0
		},
		leftWidth() {
			if (this.fullPage) {
				return 0
			}
			if (this.navigatorType === "Double Left") {
				return LEFT_WIDTH + ASIDE_WIDTH
			} else if (["Top Left", "Single Left"].includes(this.navigatorType)) {
				return ASIDE_WIDTH
			}
			return 0
		},
	},
})
