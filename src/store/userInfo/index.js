import { defineStore } from "pinia"
import MD5 from "js-md5"
import { useMenuStore } from "/store/menu"
import { useHistoryStore } from "/store/history"
import { getAxios, postAxios } from "/axios"
import { ElMessage } from "element-plus"
import Router from "../../router"
import { createRouterList } from "../../router/router"
let isGettingUserInfo = false
let waitList = []
export const useUserInfoStore = defineStore("user", {
	state() {
		return {
			userInfo: JSON.parse(window.localStorage.getItem("userInfo") || "{}"),
		}
	},
	actions: {
		//  登录
		async login(argment) {
			try {
				const { username, password } = argment
				const userInfo = await postAxios("/login?username=" + username + "&password=" + MD5(password))
				window.localStorage.setItem("refreshToken", userInfo.refreshToken || "")
				window.localStorage.setItem("token", userInfo.token || "")
				this.reloadUserInfo(true)
				createRouterList(Router)
				const menuStore = useMenuStore()
				menuStore.getMenu(true)
			} catch (error) {
				ElMessage.error(error)
			}
		},
		logout() {
			this.userInfo = {}
			Router.push("/dengLu")
			const historyStore = useHistoryStore()
			historyStore.historyList = []
			window.localStorage.removeItem("theme")
			window.localStorage.removeItem("history")
			window.localStorage.removeItem("routerList")
			window.localStorage.removeItem("refreshToken")
			window.localStorage.removeItem("token")
			window.localStorage.removeItem("userInfo")
			window.localStorage.removeItem("isShow")
			window.localStorage.removeItem("pdfjs.history")
			window.localStorage.removeItem("history")
		},
		async reloadUserInfo(refresh = false) {
			if (!refresh && this.userInfo.id) {
				return this.userInfo
			}
			const userInfo = JSON.parse(window.localStorage.getItem("userInfo") || "{}")
			if (!refresh && userInfo) {
				this.userInfo = userInfo
				return userInfo
			}
			isGettingUserInfo = true
			try {
				const userInfo = await getAxios("/userInfo")
				isGettingUserInfo = false
				this.userInfo = userInfo
				window.localStorage.setItem("userInfo", JSON.stringify(userInfo))
				waitList.forEach(item => item.resolve(this.auth))
				const menuStore = useMenuStore()
				menuStore.getMenu(true)
				return this.userInfo
			} catch (error) {
				isGettingUserInfo = false
				waitList.forEach(item => item.reject(error))
			}
		},
		getAuthList() {
			return new Promise(async (resolve, reject) => {
				if (this.auth.length) {
					resolve(this.auth)
					return
				}
				if (isGettingUserInfo) {
					waitList.push({ resolve, reject })
				} else {
					await this.reloadUserInfo()
					resolve(this.auth)
				}
			})
		},
		hasAuth(value, type = "all") {
			if (this.isSuperAdmin) {
				return true
			}
			if (this.auth.length) {
				if (typeof value === "string") {
					value = [value]
				}
				if (type === "all") {
					if (value.every(item => this.auth.includes(item))) {
						return true
					}
				} else if (type === "any") {
					if (value.some(item => this.auth.includes(item))) {
						return true
					}
				}
			}
			return false
		},
	},
	getters: {
		// 判断用户是否登录
		isLogin() {
			return !!window.localStorage.getItem("token")
			// return !!this.userInfo.id && !!window.localStorage.getItem("token")
		},
		auth() {
			return this.userInfo.permission || []
		},
		isSuperAdmin() {
			return this.userInfo.isSuperAdmin || false
		},
	},
})
