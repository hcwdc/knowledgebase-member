import { defineStore } from "pinia"
export const useSearchBarStore = defineStore("searchBar", {
	state() {
		return {
			visible: false,
		}
	},
})
