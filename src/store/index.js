import { createPinia } from "pinia"
const pinia = createPinia()
pinia.use(({ store }) => {
	store.$subscribe(({ storeId }, state) => {
		if (storeId === "setting") {
			if (state.dark) {
				document.querySelector("html")?.classList.add("dark")
			} else {
				document.querySelector("html")?.classList.remove("dark")
			}
		}
	})
})
export default pinia
