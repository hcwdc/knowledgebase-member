import { defineStore } from "pinia"
import { ASIDE_WIDTH, HEADER_HEIGHT, LEFT_WIDTH } from "./constants"
import { settings } from "nprogress"
const setting = window.DefaultGlobalSetting
if (setting.dark) {
	document.querySelector("html")?.classList.add("dark")
}
export const useSettingStores = defineStore("settings", {
	state() {
		return {
			...setting,
			fullPage: false,
			visible: false,
			isMobile: false,
			// 个人tag
			geRentags: [],
			// 管理tag
			guanLiags: [],
		}
	},
	actions: {
		setIsMobile(isMobile) {
			this.isMobile = isMobile
		},
		setGeRenTags(data) {
			this.geRentags = data
		},
		setGuanLiags(data) {
			this.guanLiags = data
		},
	},
	getters: {
		headerHeight() {
			if (this.fullPage) {
				return HEADER_HEIGHT * -1
			}
			if (["Top Left", "Narrow Top"].includes(this.navigatorType)) {
				return 0
			}
			return HEADER_HEIGHT * -1
		},
		leftCoordinate() {
			if (this.fullPage) {
				if (this.navigatorType === "Double Left") {
					return (LEFT_WIDTH + ASIDE_WIDTH) * -1
				} else if (["Top Left", "Single Left"].includes(this.navigatorType)) {
					return ASIDE_WIDTH * -1
				}
			}
			return 0
		},
		leftWidth() {
			if (this.fullPage) {
				return 0
			}
			if (this.navigatorType === "Double Left") {
				return LEFT_WIDTH + ASIDE_WIDTH
			} else if (["Top Left", "Single Left"].includes(this.navigatorType)) {
				return ASIDE_WIDTH
			}
			return 0
		},
	},
})
