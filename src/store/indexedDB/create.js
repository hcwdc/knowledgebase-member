import { indexedDBName, indexedDBVersion } from "/config"
import { createAddEditData, createIconList, createUploadFile } from "./createStore"
export function createIndexedDB() {
	return new Promise((resolve, reject) => {
		const indexedDB = window.indexedDB
		const database = indexedDB.open(indexedDBName, indexedDBVersion)
		database.onerror = () => {
			reject(new Error("createIndexedDB error"))
		}
		database.onsuccess = function () {
			resolve(this.result)
		}
		database.onupgradeneeded = function () {
			createAddEditData(this.result)
			createUploadFile(this.result)
			createIconList(this.result)
			resolve(this.result)
		}
	})
}
