export function createAddEditData(database) {
	const addEdit = database.createObjectStore("add-edit", {
		keyPath: "id",
	})
	addEdit.createIndex("date", "date", { unique: false })
	addEdit.createIndex("formData", "formData", { unique: false })
	addEdit.createIndex("data", "data", { unique: false })
}
export function createIconList(database) {
	const uploadFile = database.createObjectStore("icons", {
		keyPath: "id",
		autoIncrement: true,
	})
	uploadFile.createIndex("url", "url", { unique: false })
}
export function createUploadFile(database) {
	const uploadFile = database.createObjectStore("upload-file", {
		keyPath: "id",
		autoIncrement: true,
	})
	uploadFile.createIndex("name", "name", { unique: false })
	uploadFile.createIndex("url", "url", { unique: false })
	uploadFile.createIndex("path", "path", { unique: false })
	uploadFile.createIndex("size", "size", { unique: false })
	uploadFile.createIndex("lastModified", "lastModified", { unique: false })
	uploadFile.createIndex("md5", "md5", { unique: false })
}
