import { onUnmounted, ref } from "vue"

export function useResize(size = 768) {
	const isPc = ref(!(window.innerWidth <= size))

	onUnmounted(() => {
		window.removeEventListener("resize", resize)
	})

	function resize() {
		isPc.value = !(window.innerWidth <= size)
	}
	resize()

	window.addEventListener("resize", resize)

	return {
		isPc,
	}
}
