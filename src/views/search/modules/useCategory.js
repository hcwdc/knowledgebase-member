import { computed, ref } from "vue"
import { getAxios } from "/axios"

export function useCategory(formData, onSearch) {
	const category = ref([])
	const url = computed(() => {
		return formData.isVideo == 2
			? "/knowledge/kbSubjectWarehouse/findByListToCategory"
			: `/knowledge/kbSubjectWarehouse/findByListToCategoryVideo?isSearch=1`
	})

	async function getCategory() {
		const result = await getAxios(url.value)
		category.value = formData.isVideo == 1 ? [result] : result
	}
	function onCategoryCheck(_event, { checkedNodes }) {
		formData.fileCategorys = checkedNodes.map(item => item.id).join()
		onSearch()
	}

	getCategory()

	return {
		category,
		getCategory,
		onCategoryCheck,
	}
}
