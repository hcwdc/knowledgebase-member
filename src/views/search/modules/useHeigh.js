import { ref } from "vue"
import { useFormatDate } from "/utils/useDate.js"

const week = () => {
	const d = new Date()
	const sd = new Date(d.getTime() - 7 * 24 * 60 * 60 * 1000)
	return [useFormatDate(sd, "yyyy-MM-dd 00:00:00"), useFormatDate(d, "yyyy-MM-dd 23:59:59")]
}
// 月
const month = () => {
	const d = new Date()
	const sd = new Date(d.getTime() - 30 * 24 * 60 * 60 * 1000)
	return [useFormatDate(sd, "yyyy-MM-dd 00:00:00"), useFormatDate(d, "yyyy-MM-dd 23:59:59")]
}
// 年
const year = () => {
	const d = new Date()
	const sd = new Date(d.getTime() - 365 * 24 * 60 * 60 * 1000)
	return [useFormatDate(sd, "yyyy-MM-dd 00:00:00"), useFormatDate(d, "yyyy-MM-dd 23:59:59")]
}

export function useHeigh(formData) {
	const visible = ref(false)

	const date = computed({
		get() {
			return [formData.startTime, formData.endTime]
		},
		set(date) {
			if (date) {
				;[formData.startTime, formData.endTime] = date
			} else {
				;[formData.startTime, formData.endTime] = ["", ""]
			}
		},
	})

	function onOpenHeigh() {
		visible.value = true
	}
	function onChangeFileType(fileType) {
		formData.fileType = fileType
	}

	function resetting() {
		date.value = null
		formData.fileType = ""
	}

	function onQuickyDate(type) {
		switch (type) {
			case "month":
				date.value = month()
				break
			case "year":
				date.value = year()
				break
			default:
				date.value = week()
				break
		}
	}

	return {
		heighVisible: visible,
		onOpenHeigh,
		date,
		onQuickyDate,
		onChangeFileType,
		resetting,
	}
}
