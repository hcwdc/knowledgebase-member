import { computed, ref } from "vue"
import { getAxios } from "/axios"

export function useHot() {
	const hotList = ref([])

	async function getData() {
		hotList.value = await getAxios("/knowledge/kbModelLabel/listHotWord")
	}
	getData()
	return { hotList }
}
