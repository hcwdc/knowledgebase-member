export function parseKeyPath(data, path) {
	try {
		if (/^((\!{0,2})data(\??))\./.test(path)) {
			return eval(path)
		}
		return eval(`data?.${path}`)
	} catch (error) {
		console.log(error)
		return ""
	}
}
export function hump2short(name = "") {
	if (!name) return ""
	name = name[0].toLowerCase() + name.substring(1)
	return name.replace(/([A-Z])/g, (_match, p1) => "-" + p1.toLowerCase())
}
export function short2hump(name = "") {
	if (!name) return ""
	return name.replace(/(\-([a-z]))/g, (_match, _p1, p2) => p2.toUpperCase())
}
export function short2Hump(name = "") {
	if (!name) return ""
	name = name[0].toUpperCase() + name.substring(1)
	return name.replace(/(\-([a-z]))/g, (_match, _p1, p2) => p2.toUpperCase())
}
