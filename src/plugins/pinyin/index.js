import Pinyin from "./ChinaPinYin"
const savePinyin = {}
export function findPinYin(cn, pinyin) {
	if (!/^[a-zA-Z]+$/.test(pinyin)) {
		return new RegExp(pinyin, "i").test(cn)
	}
	let JP = ""
	let QP = ""
	let HP = ""
	if (savePinyin[cn]) {
		JP = savePinyin[cn].JP
		QP = savePinyin[cn].QP
		HP = savePinyin[cn].HP
	} else {
		savePinyin[cn] = {
			JP: Pinyin.GetJP(cn),
			QP: Pinyin.GetQP(cn),
			HP: Pinyin.GetHP(cn),
		}
		JP = savePinyin[cn].JP
		QP = savePinyin[cn].QP
		HP = savePinyin[cn].HP
	}
	const PinYinRegExp = new RegExp(pinyin, "i")
	return PinYinRegExp.test(JP) || PinYinRegExp.test(QP) || PinYinRegExp.test(HP)
}
