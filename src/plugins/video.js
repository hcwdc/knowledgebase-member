export function useCutFirstVideoFrame(videoDom) {
	videoDom = typeof videoDom === "string" ? document.querySelector(videoDom) : videoDom

	videoDom.currentTime = 0.1

	return new Promise((resolve, reject) => {
		videoDom.addEventListener("canplay", function () {
			const canvas = document.createElement("canvas")
			canvas.width = videoDom.videoWidth
			canvas.height = videoDom.videoHeight

			const ctx = canvas.getContext("2d")
			ctx.drawImage(videoDom, 0, 0, canvas.width, canvas.height)

			canvas.toBlob(blob => {
				resolve([blob, canvas.toDataURL("image/png"), videoDom])
			})
		})
		videoDom.addEventListener("error", function (e) {
			reject(e)
		})
	})
}
