export function forwardZero(num, length = 1) {
	return num < 10 ? Array(length).join("0") + num : num + ""
}
export function makeUpZero(num, length) {
	return +(num + Array(length).join("0")).slice(0, length)
}
export function calcDateDay(fDate = new Date(), lDate = new Date()) {
	var date1 = new Date(fDate)
	var date2 = new Date(lDate)
	var timeDiff = Math.abs(date2.getTime() - date1.getTime())
	var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24))
	return diffDays
}
export const isLeapYear = year => {
	year = +year
	return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0
}
export function getMonthDay(date) {
	date = new Date(date)
	const monthDay = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
	if (date === "Invalid Date") {
		console.error("getMonthDay: 日期格式错误")
		return null
	}
	const month = date.getMonth()
	if (month == 1 && isLeapYear(date.getFullYear())) {
		return 29
	}
	return monthDay[month]
}
export function formattingTimestamps(timestamps = +new Date()) {
	timestamps = timestamps + ""
	if (timestamps.length < 13) {
		timestamps = makeUpZero(+timestamps, 13)
	} else if (timestamps.length > 13) {
		timestamps = timestamps.slice(0, 13)
	}
	return +timestamps
}
export function formatDate(date, fmt) {
	if (typeof date === "string") {
		date = date.replace(/-/g, "/")
	} else if (typeof date === "number") {
		date = formattingTimestamps(date)
	}
	const time = new Date(date)
	const year = time.getFullYear()
	const month = time.getMonth() + 1
	const day = time.getDate()
	const hour = time.getHours()
	const minute = time.getMinutes()
	const second = time.getSeconds()
	return fmt.replace(/YYYY|yyyy|YY|yy|MM|M|DD|dd|D|d|hh|h|HH|H|mm|m|ss|s|SS|S/g, a => {
		switch (a) {
			case "yy":
			case "YY":
				return (year + "").slice(2)
			case "yyyy":
			case "YYYY":
				return year + ""
			case "M":
				return month + ""
			case "MM":
				return forwardZero(month)
			case "d":
			case "D":
				return day + ""
			case "dd":
			case "DD":
				return forwardZero(day)
			case "h":
			case "H":
				return hour + ""
			case "hh":
			case "HH":
				return forwardZero(hour)
			case "m":
				return minute + ""
			case "mm":
				return forwardZero(minute)
			case "s":
			case "S":
				return second + ""
			case "ss":
			case "SS":
				return forwardZero(second)
			default:
				return a
		}
	})
}
