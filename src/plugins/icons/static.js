const icons = {
	"address": "png",
	"copyright": "svg",
	"icon-excel": "png",
	"icon-markdown": "png",
	"icon-pdf": "png",
	"icon-ppt": "png",
	"icon-text": "png",
	"icon-unknown": "png",
	"icon-word": "png",
	"icon-zip": "png",
	"le-tabbar": "svg",
	"reduce": "svg",
}

export default icons