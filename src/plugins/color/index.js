export function adjustColor(color, range) {
	if (/^rgb/.test(color)) {
		const RgbValueArry = color
			.replace(/(r|g|b|a)/gi, "")
			.replace(/\(|\)/g, "")
			.split(",")
		const R = parseInt(RgbValueArry[0]).toString(16)
		const G = parseInt(RgbValueArry[1]).toString(16)
		const B = parseInt(RgbValueArry[2]).toString(16)
		color = `#${R}${G}${B}`
	}
	let newColor = "#"
	for (let i = 0; i < 3; i++) {
		const hxStr = color.substr(i * 2 + 1, 2)
		let val = parseInt(hxStr, 16)
		val += range
		if (val < 0) val = 0
		else if (val > 255) val = 255
		newColor += val.toString(16).padStart(2, "0")
	}
	return newColor
}
export function colorShade(color) {
	let RgbValueArry = []
	if (/^rgb/.test(color)) {
		RgbValueArry = color
			.replace(/(r|g|b|a)/gi, "")
			.replace(/\(|\)/g, "")
			.split(",")
	} else if (/^#/.test(color)) {
		if (color.length != 7) return new Error("颜色长度不正确")
		color = color.replace("#", "")
		RgbValueArry = color.match(/\w{2}/g)
		RgbValueArry = RgbValueArry.map(item => parseInt(item, 16) + "")
	}
	var grayLevel = +RgbValueArry[0] * 0.299 + +RgbValueArry[1] * 0.587 + +RgbValueArry[2] * 0.114
	return grayLevel >= 192 ? "white" : "black"
}
